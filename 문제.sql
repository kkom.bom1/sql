--조인
/*1. LOCATIONS 및 COUNTRIES 테이블을 사용하여 HR 부서를 위해 모든 부서의 주소를 생성하는 query를 작성하시
오. 출력에 위치 ID, 주소, 구/군, 시/도 및 국가를 표시하며, NATURAL JOIN을 사용하여 결과를 생성합니다.*/
SELECT d.department_id, location_id, l.street_address, l.city, l.state_province, c.country_name
FROM locations l NATURAL JOIN countries c
                 NATURAL JOIN departments d
ORDER BY department_id;



--서브쿼리
/*1. <Zlotkey와 동일한 부서>에 속한 모든 사원의 이름과 입사일을 표시하는 질의를 작성하시오. Zlotkey는 결과에서
제외하시오.*/
--1)
SELECT department_id
FROM employees
WHERE INITCAP(last_name) = 'Zlotkey';
--2)
SELECT first_name, hire_date
FROM employees
WHERE department_id = (SELECT department_id
                        FROM employees
                        WHERE INITCAP(last_name) = 'Zlotkey')
  AND INITCAP(last_name) <> 'Zlotkey';
