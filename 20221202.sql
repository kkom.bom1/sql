--獣碇什(収切幻 亜管) -> 角獄展脊拭辞幻 紫遂亜管
CREATE SEQUENCE y_emp_seq; 
--奄沙室特精 1採斗 獣拙背辞 1梢 装亜
INSERT INTO y_emp VALUES (y_emp_seq.NEXTVAL, 'Kang', 'Ho_Dong', null);
SELECT * FROM y_emp; --insert馬蟹 叔鳶馬檎 舛雌旋生稽 叔楳鞠壱 継拷鞠澗暗艦 戚耕 1戚 叔楳鞠壱 2亜 蟹臣呪 赤陥.

SELECT y_emp_seq.CURRVAL FROM dual;

--INSERT INTO y_emp VALUES (y_emp_seq.NEXTVAL, ?, ?, ?)  -- PK葵聖 切疑装亜 獣徹奄 是背 紫遂
--INSERT INTO y_emp VALUES (SYSDATE, ?, ?, ?) -- 葵聖 左鎧爽澗 員拭 SYSDATE窮 獣碇什, 敗呪去聖 紫遂背亀吉陥.

DROP SEQUENCE y_emp_seq;

CREATE TABLE y_dept(
    id NUMBER(7,0) PRIMARY KEY,
    name VARCHAR2(25) 
);

CREATE TABLE y_emp(
    id NUMBER(7,0) PRIMARY KEY,
    last_name VARCHAR2(25),
    first_name VARCHAR2(25),
    dept_id NUMBER(7,0),
    CONSTRAINT y_emp_dept_id_fk FOREIGN KEY ( dept_id )
        REFERENCES y_dept(id)
);

CREATE SEQUENCE y_emp_seq
       INCREMENT BY 5 --5梢 装亜
       START WITH 100 --100採斗 獣拙
       MAXVALUE 120 --置企 120猿走
       CYCLE --歎採斗 獣拙(歎 獣拙拝凶 100採斗 獣拙馬壱 粛生檎 MINVALUE 100)
       NOCACHE;
SELECT y_emp_seq.CURVAR FROM dual; -- 焼送 疑拙聖 照背辞 薄仙葵聖 硝呪蒸陥.

INSERT INTO y_emp VALUES(y_emp_seq.NEXTVAL, 'Kim', 'Han-sol', null);
SELECT * FROM y_emp; 

CREATE SEQUENCE y_emp_seq_new
       INCREMENT BY 5 
       START WITH 100 -- 獣拙繊
       MAXVALUE 120 
       MINVALUE 100
       CYCLE  --MINVALUE亜 蒸製 1採斗 獣拙
       NOCACHE;
DELETE FROM y_emp;

INSERT INTO y_emp VALUES(y_emp_seq_new.NEXTVAL, 'Kim', 'Han-sol', null);  --pk拭 杏鍵陥. (湛獣拙戚 100 => 掻差鞠奄凶庚)
SELECT * FROM y_emp; 

--獣碇什 呪舛
ALTER SEQUENCE y_emp_seq_new
        NOMINVALUE;

--獣碇什 舛左 左奄
SELECT sequence_name, min_value, max_value, increment_by, last_number
FROM user_sequences;

--獣碇什澗 琶推馬檎 紫遂馬澗依. -> 獣碇什澗 砺戚鷺引 尻衣吉依戚 蒸製
--獣碇什 => 析舛廃 鋭帳 政走廃澗依精 疏走幻 琶呪稽 紫遂拝 琶推亜 蒸陥.

/*
砺戚鷺 => 亜舌 拙壱, 琶呪旋昔 糎仙
砺戚鷺聖 奄鋼生稽 昔畿什 持失
砺戚鷺聖 奄鋼生稽 獣碇什 坂 疑税嬢亜 持失
砺戚鷺聖 肢薦馬檎 益 砺戚鷺税 薦鉦繕闇生稽 紫遂鞠澗 昔畿什澗 旭戚 肢薦
獣碇什, 坂, 疑税嬢澗 肢薦 いい
  坂 -> 据沙砺戚鷺 奄鋼生稽 衣引 繕噺 -> 持失吉依戚 切疑 肢薦 いい /紫遂 災亜管 ->疑析廃 戚硯 鎮軍税 砺戚鷺 幻級嬢走檎 疑拙
  疑戚嬢 -> 据沙砺戚鷺聖 企端馬澗 戚硯 -> 旭戚 肢薦 いい/紫遂災亜管  ->疑析廃 戚硯 鎮軍税 砺戚鷺 幻級嬢走檎 疑拙
  獣碇什 -> 据沙砺戚鷺引 尻淫失戚 蒸製 (尻衣失 いい) -> 旭戚 肢薦 いい/ 仙紫遂 亜管
*/
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--疑税嬢
CREATE SYNONYM emp_info
FOR y_emp;

--据沙汽戚斗dml, 坂持失 亜管
SELECT * FROM emp_info;
DELETE FROM emp_info;

--疑税嬢 舛左 左奄
SELECT synonym_name, table_owner, table_name
FROM user_synonyms;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--獣什奴 映廃
/* DCL
grant => 映廃採食
  + with admin => 映廃 閤精 紫寓戚 陥献紫寓拭惟 映廃 匝呪 赤製
                  (据掘澗 sys幻 映廃 匝 呪 赤製)
                  噺呪獣 尻戚精 映廃精 旦噺鞠走 省澗陥. (鯵紺旋生稽 達焼亜辞 映廃 噺呪 背醤廃陥 )=> 爽税琶推
revoke => 映廃旦噺
映廃 切端 呪舛精 蒸陥.
搾腔腰硲 痕井精 亜管 
*/

--'+' -> sys/oravle 継->sysdba
--sys域舛 羨悦 板 歯稽錘 域舛
CREATE USER ktk --焼戚巨
IDENTIFIED BY test; --搾腰

--映廃 採食(TO - ~拭惟 層陥)
GRANT CREATE SESSION
 TO ktk;
 --run sql => conn ktk/test 
 -- DBA羨紗幻 吃屍 焼巷依亀 映廃 いい 

--砺戚鷺 幻級奄, 霜税 映廃 採食  => 戚係惟 繕杯聖 搭背 映廃 採食
GRANT CREATE TABLE, SELECT ANY TABLE
TO ktk;

--映廃 旦噺(FROM - ~稽採斗 晒�f陥)
REVOKE SELECT ANY TABLE
FROM ktk;


--古腰 乞窮 域舛 馬蟹馬蟹 映廃爽奄 毘級陥. => 継戚虞澗 鯵割
--継 : 室採旋生稽 蟹寛閃赤澗 映廃 陥獣 繕杯背辞 戚硯 採食( 唖 紫寓 送厭拭 魚虞 馬蟹馬蟹 匝琶推蒸戚 継 爽檎喫)

--映廃 左奄
SELECT * FROM user_sys_privs WHERE username = 'ktk';

--継
CREATE ROLE emp_manager;

GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW
TO emp_manager;

GRANT emp_manager
TO ktk;

DROP ROLE emp_manager;

--------------------------------------------------------------------------------
--梓端映廃
GRANT SELECT ON hr.employees
TO ktk; -- hr税 employees税 砺戚鷺傾 漆刑闘 亜管

GRANT UPDATE (first_name, phone_number)
ON hr.employees
TO ktk;

GRANT SELECT  --  GRANKT ALL -> 砺戚鷺 梓端映廃 陥 爽澗暗
ON hr.job_history
TO PUBLIC;

--------------------------------------------------------------------------------
--映廃 旦噺
REVOKE ALL
ON hr.job_history
FROM public;

REVOKE UPDATE
ON hr.employees
FROM ktk;

REVOkE SELECT 
ON hr.employees
FROM ktk;