--문제1)
--같은 컬럼명이 없다면 접두어는 안써도 된다.
SELECT department_id 부서번호, department_name 부서이름, city 도시명
FROM departments d JOIN locations l
  ON d.location_id = l.location_id;
  
--------------------------------------------------------------------------------
--문제4)
--서브쿼리)
SELECT department_id
FROM departments
WHERE UPPER(department_name) = 'IT';

--메인쿼리)
SELECT employee_id 사원번호, first_name 이름, job_id 업무
FROM employees
WHERE department_id = (SELECT department_id
                    FROM departments
                    WHERE UPPER(department_name) = 'IT');
                    
--------------------------------------------------------------------------------
--문제 6-1)
INSERT INTO prof (profno, name, id, hiredate)
VALUES(1003, 'Adam', 'a1003', TO_DATE('11/03/02', 'yy/MM/dd'));

--------------------------------------------------------------------------------
--문제7)
--(1)
CREATE INDEX prof_name_idx
ON prof (name); --인덱스는 존재하는 컬럼으로 만들어짐
                --컬럼 여러개 가능
                
--(4)
ALTER TABLE prof
MODIFY name VARCHAR(20); -- 널값이 있는데 nn추가 안됨
                         -- 기존에 있는 값보다 큰 값만 수정가능
                         
--------------------------------------------------------------------------------
--문제9)
DROP TABLE prof PURGE;
DROP TABLE prof;

