/* 
서브쿼리 -> 실제 사용하고자 하는 쿼리 내 존재하는 또다른 쿼리
메인쿼리 -> 서브쿼리 품고 있는 쿼리

WHERE, HAVIN => 비교하는 대상에 서브쿼리, 연산자 뒤에 존재, ()로 서브쿼리라고 표시
                서브쿼리 실행 -> 결과를 리턴받을 메인쿼리 실행 (동시실행 X)

서브쿼리 
-단일 행 : 결과 1개  - 하나의 컬럼을 기반 
-다중 행 : 결과 2개 이상  - 하나의 컬럼을 기반
          IN : OR의 연산자 
          ANY(or-합집합 의미) : 이중에 어떤 것 + >(최소한 이중 최소값보다는 크다)/<(최소한 이중 최대값보다는 작다)/=(IN이랑 같은의미) 
          (SOME : ANY와 비슷)
          ALL(and-교집합 의미) : 모두 어떤 것 + >(최소한 이중 최대값보다는 크다)/<(최소한 이중 최소값보다는 작다.)
-다중 열 :[PAIRWISE] 결과 쌍(한쌍 단위로 움직임) -> 컬럼이 2개이상 구성
                                      ex) 부서번호, 최소급여 = 부서번호(100), 최소급여(2000) 
                                                             부서번호(200), 최소급여(3000)
                                          부서번호가 200이면서 최소급여가 2000일때 X -> 한 줄(쌍)로 통째로 움직임
*/

--------------------------------------------------------------------------------
--다중행 서브쿼리
--IN) 부서별로 최고 급여를 받은 사원의 이름, 직급, 부서, 급여를 출력
--1) 부서별 최고 급여
SELECT MAX(salary)
FROM employees
GROUP BY department_id;
--2)
SELECT first_name, job_id, department_id, salary
FROM employees
WHERE salary IN (SELECT MAX(salary)
                FROM employees
                GROUP BY department_id);

--ANY) 업무 중 SH_CLERK을 수행하는 직원들 중에서 
--      ST_CLERK을 수행하는 직원의 최소 급여보다 많이 받는 직원의 사원번호, 이름, 급여 출력
SELECT employee_id, first_name, salary
FROM employees
WHERE job_id = 'SH_CLERK'
  AND salary >ANY (SELECT salary
                   FROM employees
                   WHERE job_id = 'ST_CLERK')
ORDER BY salary;
 
--ALL) 업무중 SH_CLERK을 수행하는 직원들 중에서 
--     ST_CLERK을 수행하는 직원의 최대급여보다 많이 받는 직원의 사원번호, 이름, 급여 출력
SELECT employee_id, first_name, salary
FROM employees
WHERE job_id = 'SH_CLERK'
  AND salary >ALL (SELECT salary
                   FROM employees
                   WHERE job_id = 'ST_CLERK')
ORDER BY salary;
 
/*
>ANY : >MIN / <ANY : <MAX
>ALL : >MAX / <ALL : <MIN
*/
--------------------------------------------------------------------------------
--다중열 서브쿼리
-- 부서번호 30번인 부서에서 일하는 각 사원들의 급여와 커미션이 같은 직원들의 이름과 업무, 급여, 커미션 출력
--PAIRWISE( in으로 두개의 컬럼 한꺼번에 비교)
--1) 서브쿼리
SELECT salary, NVL(commission_pct, 0) -- null끼리는 같다의 개념이 없다
FROM emp
WHERE department_id = 30;

--2)메인쿼리 ??????????????????????
SELECT first_name, job_id, salary, NVL(commission_pct,0)
FROM emp
WHERE (salary, NVL(commission_pct,0)) IN (SELECT salary, NVL(commission_pct, 0) 
                                            FROM emp
                                            WHERE department_id = 30)
  AND department_id <> 30
ORDER BY salary DESC;

--NON-PAIRWISE (각 컬럼을 개별로 비교 - 한쌍으로 안움직여서 pairwise와 다를수 있다.) ??????????????????
SELECT first_name, job_id, salary, NVL(commission_pct,0)
FROM emp
WHERE salary IN (SELECT salary 
                 FROM emp
                 WHERE department_id = 30)
  AND NVL(commission_pct, 0) IN (SELECT NVL(commission_pct, 0)
                                 FROM emp
                                 WHERE department_id = 30)
  AND department_id <> 30
ORDER BY salary DESC;

------???????????????????????
DROP TABLE emp;
CREATE TABLE emp
AS
 SELECT first_name, job_id, salary, commission_pct, department_id
 FROM employees;
INSERT INTO emp VALUES('Ed', 'IT_PROG', 1000, 0.3, 30);
COMMIT;
------

--현재사원들 중 급여가 가장 높은 순으로 해서 1등에서 10등까지 업무, 입사일, 급여를 출력
-- 1) 서브쿼리 
SELECT first_name, job_id, hire_date, salary
FROM  employees
ORDER BY salary DESC;
--2) 메인쿼리
-- ROWNUM, -> 각 행의 넘버를 붙여줌/ 무엇을 기준으로 넘버링 하고 싶은지 기반으로 써야한다.
SELECT ROWNUM , s.*  -- *를 써도 컬럼이 4개만 보인다.(FROM절의 서브쿼리가 네개의 컬럼을 가지니까)
                     -- *에 S별칭을 쓴건 ROWNUM때문임
FROM(SELECT first_name, job_id, hire_date, salary
     FROM  employees
     ORDER BY salary DESC) s   
WHERE ROWNUM BETWEEN 1 AND 10;

--SELECT 절에 언급이 안돼도 쓸수있는 컬럼의 제한은 없지만
--서브쿼리는 서브쿼리를 실행했을때의 결과만을 쓸수있다.  =>FROM 절에 쓸때 주의 (가져온 컬럼만 사용가능)

--------------------------------------------------------------------------------
--DML
/* 무결성 제약조건 
1. 정확성 : 최소한의 중복, 누락X
2. 일관성 : 원인이 있으면 그에따른 결과가 계속 유지돼야한다
3. 유효성 : 각 컬럼의 적절한 값이 들어가야한다.
*/
--무결성 제약조건 오류 
DELETE FROM departments
WHERE department_id = 30; -- 외래키 삭제하려함

UPDATE employees
SET department_id = 1234 --부모키를 찾을 수 없음
WHERE department_id = 30; 
--** 외래키는 무조건 식별키(프라이머리키)로 되어있는 걸로 해줘야한다

--새로운 테이블 만들기
DROP TABLE emp_test; -- 테이블 자체 지우기
CREATE TABLE emp_test(
    emp_id NUMBER(5),
    emp_name VARCHAR2(10),
    mgr NUMBER(5),
    sal NUMBER(7, 2),
    dept_id NUMBER(2),
    h_date DATE
);
--------------------------------------------------------------------------------
--[INSERT]
INSERT INTO emp_test (emp_id, emp_name) VALUES (100, 'Kil-Dong');
INSERT INTO emp_test VALUES (110, 'Han-Kwon', '', 3000, null, sysdate);  --''은 오라클에만 가능
INSERT INTO emp_test VALUES (120, USER, 2500, 10);  -- 값이 충분하지 않음(전체를 호출했으면 전체 다 입력해 줘야함) 
INSERT INTO emp_test VALUES (120, USER, 100, 2500, 10, TO_DATE('2020-10-25', 'yyyy-MM-dd')); --USER : 호출하는 순간 이 세션을 사용하고 있는 계정의 정보를 반환 
                                                                                            --       체크 조건을 걸면 안된다./ 일반적으로 값을 집어넣을때 사용
                                                        
--VALUES에 서브쿼리 사용 (단일행만가능)
INSERT INTO emp_test (emp_id, emp_name, mgr)
VALUES (130, 'Yun-Han', (SELECT emp_id FROM emp_test WHERE dept_id = 10) );

--기존 employees테이블을 서브쿼리로 사용(VAUSES 사용 ㄴㄴ) => 원하는 양만큼 insert 가능
INSERT INTO emp_test 
  SELECT employee_id, first_name, manager_id, salary, department_id, hire_date --emp_test의 컬럼수만큼 가져와야한다.
  FROM employees 
  WHERE department_id =30;
  
SELECT * FROM  emp_test;
COMMIT; --저장
--------------------------------------------------------------------------------
--[DELETE]
DELETE FROM emp_test WHERE emp_id = 0; --없는 컬럼 삭제/수정 => 오류는 안나지만 그 결과가 삭제/수정된 테이블 0개
DELETE FROM emp_test; --WHERE 절없으면 내부 데이터 모두 삭제

--매니저의 사원번호가 114번인 모든 사원의 정보를 삭제하라
--다중행 서브쿼리
DELETE emp_test -- FROM 생략가능
WHERE emp_id IN (SELECT emp_id
                      FROM emp_test
                      WHERE mgr = 114);
                      
--매니저의 이름이 DEN인 모든 사원의 정보를 삭제하라                      
--단일행 서브쿼리
DELETE emp_test
WHERE mgr = (SELECT emp_id FROM emp_test WHERE UPPER(emp_name) = 'DEN');

ROLLBACK; -- 저장한거 불러오기
SELECT * FROM  emp_test;

--------------------------------------------------------------------------------
--[UPDATE]
UPDATE emp_test SET sal = sal * 1.1;  -- 모든 사원의 급여 10퍼 인상
UPDATE emp_test SET h_date = sysdate;  -- WHERE절 없음 전체 수정

UPDATE emp_test SET sal = sal * 1.2 WHERE dept_id = 30;

--길동 컬럼 한꺼번에 채우기
UPDATE emp_test SET sal = 15000, h_date = TO_DATE('2010년', 'YYYY"년"') WHERE emp_id = 100;

--SET절에 서브쿼리 사용(단일행만 가능)
UPDATE emp_test
SET sal = (SELECT salary FROM employees WHERE employee_id = emp_id)
WHERE emp_id BETWEEN 100 AND 120;

ROLLBACK;
SELECT * FROM  emp_test;

--------------------------------------------------------------------------------
SELECT first_name, SUBSTR(TRUNC(salary,-2), 1, length(salary)-2)||'■', salary
FROM employees;

-------------------------------------------------------------------------------
ROLLBACK;
SELECT *FROM emp_test;

INSERT INTO emp_test
    SELECT employee_id, first_name, manager_id, salary, department_id, hire_date
    FROM employees
    WHERE department_id = 10;

SAVEPOINT a;

UPDATE emp_test
SET h_date = ADD_MONTHS(sysdate, 6);

ROLLBACK TO SAVEPOINT a;  --commit이 아님 저장이 안된다. => 완전히 반영할거면 커밋해야한다
DROP TABLE emp_test;  -- ddl/dcl과 dml은 함께쓰지말자

ROLLBACK;