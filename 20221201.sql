--테이블 생성
--create table => 빈 테이블 생성
CREATE TABLE sample_tbl ( --()안에 컬럼의 정보
    sam1 VARCHAR2(20), --byte은 생략가능 VARCHAR2(20)/ 
--  sam1 VARCHAR2(20, CHAR) --char은 한글자한글자 기준(한글은 char을 쓰는게 좋다.) 
    sam2 NUMBER(9,2),
    sam3 DATE
);

SELECT * FROM sample_tbl;

--데이터 딕셔너리
SELECT *
--FROM user_tables;  --유저가 들고있는 테이블의 정보 
FROM all_tables; -- 테이블의 모든 정보(오라클에 등록되어있는 테이블의 정보)

DROP TABLE sample_tbl; 
--DELETE : 테이블 내부 행을 지우는거/ DROP : 테이블 전체 지우는거

--------------------------------------------------------------------------------
--서브쿼리 사용(AS ~) 
--서브쿼리 해서 테이블 생성할떄 제약조건 빠지고 생성된다. (not null만 유지됨)
--기존의 데이터를 따로 관리하고자 할떄
CREATE TABLE emp_dept30 (emp_id, f_name, dept)  --만들고자 하는 테이블뒤 ()안 컬럼 이름 새롭게 지정 가능
AS 
    SELECT employee_id, first_name, department_id
    FROM employees
    WHERE department_id = 30;

SELECT * FROM emp_dept30;
DROP TABLE emp_dept30;


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--테이블수정
--추가
ALTER TABLE emp_dept30
ADD (
    hire_date DATE DEFAULT sysdate, -- 추가할때 디폴드값을 포함하면 기존의 데이터가 디폴드값 적용될수 있다.
    manager_id NUMBER(5,0), -- 추가할때 기존 컬럼의 추가된 행의 값은 null
    salary NUMBER(9,0)
);

--수정
ALTER TABLE emp_dept30
MODIFY manager_id VARCHAR2(10);  --기존에 값이 없으니 변경 가능

--테이블 구조 보기
DESCRIBE emp_dept30;
--INSERT
INSERT INTO emp_dept30 VALUES(123, 'Dell', 50 , TO_DATE('2022-12-01', 'yyyy-MM-dd'), 'Alex', 5233);
COMMIT;

ALTER TABLE emp_dept30
MODIFY (
    manager_id NUMBER(5,0),  --하나의 값이라도 들어가있는 상태엔 데이터 타입 변경 불가
    salary NUMBER (5, 0)
);

ALTER TABLE emp_dept30
MODIFY (
    manager_id VARCHAR2(12),
    salary NUMBER (5, 0)   -- 크기 줄일땐 값이 있는 상태에서는 줄일수 없음
);

--삭제
ALTER TABLE emp_dept30
DROP COLUMN manager_id;  --COLUMN은 생략가능 (DROP manager_id) / 기존의 값이 있어도 다 삭제됨

--set unused
ALTER TABLE emp_dept30
SET UNUSED (salary);

--사용하지 않는 컬럼 확인
SELECT * FROM user_unused_col_tabs;

--사용하지 않는 컬럼 드랍
ALTER TABLE emp_dept30
DROP UNUSED COLUMNS;  -- SET UNUSED된 모든 컬럼들을 삭제

SELECT * FROM emp_dept30;

--------------------------------------------------------------------------------
--테이블 삭제
--DROP
DROP TABLE emp_dept30;


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--제약조건

CREATE TABLE parent_tbl(
    p_id NUMBER(5), 
    --p_id NUMBER(5) UNIQUE, -- 개별로 제약조건 
    p_name VARCHAR(10) NOT NULL,
    p_birth DATE,
    p_gender CHAR(1),
    CONSTRAINT parent_p_birth_uk UNIQUE (p_id, p_birth) -- 테이블이름_컬럼이름_제약조건이름 실제제약조건(컬럼)
                                                        -- 각각 개별로 uk주지 않고 하나의이름으로 두개의 컬럼에 한꺼번에 uk를 주면 그 컬럼 한쌍별로 다르면 가능
                                                        -- 개별로 하려면 2개의 제약조건 이름으로 처리해야한다.( CONSTRAINT parent_p_id_uk UNIQUE (p_id)
                                                        --                               CONSTRAINT parent_p_birth_uk UNIQUE (p_birth))
);

--INSERT INTO parent_tbl(p_id) VALUES(1000); -- nn에는 무조건 값을 넣어줘야한다.
INSERT INTO parent_tbl (p_name) VALUES('김나나');


INSERT INTO parent_tbl(p_id, p_name, p_birth) VALUES (1000, '김나나', '2022-12-01');
INSERT INTO parent_tbl(p_id, p_name, p_birth) VALUES (2000, '김나나', '2022-12-01');
INSERT INTO parent_tbl(p_id, p_name, p_birth) VALUES (1000, '김나나', '2022-12-02');  
-- 테이블 별로 uk를 줘서 개별로 중복되어도 한쌍(p_id, p_birth)으로는 중복안되니까 가능

SELECT * FROM parent_tbl;
DROP TABLE parent_tbl;

--------------------------------------------------------------------------------
CREATE TABLE parent_tbl(
    p_id NUMBER(5, 0),
    p_name VARCHAR2(10),
    p_gender CHAR(1),
    CONSTRAINT parent_p_id_pk PRIMARY KEY (p_id, p_name) --복합키
);

INSERT INTO parent_tbl (p_id, p_name) VALUES (1000, '한나나');  
INSERT INTO parent_tbl (p_id, p_name) VALUES (1000, '한다다');  
INSERT INTO parent_tbl (p_id, p_name) VALUES (2000, '한다다');  
INSERT INTO parent_tbl (p_id, p_name) VALUES (2000, '한나나');  
-- 테이블 별로 pk를 줘서 개별로 중복되어도 한쌍(p_id, p_name)으로는 중복안되니까 가능

SELECT * FROM parent_tbl;
DROP TABLE parent_tbl;

--------------------------------------------------------------------------------
CREATE TABLE child_tbl(
    c_id NUMBER(5),
    c_phone VARCHAR2(15) DEFAULT 'OOO' NOT NULL UNIQUE,  -- 식별용도 NO 
    p_id NUMBER(7, 2), -- NUMBER(5, 0)
    p_name VARCHAR2(9), -- VARCHAR2(10) //실제 입력하면 문제 - 외래키는 부모를 다 담을 정도는 돼야한다.
    CONSTRAINT child_p_id_p_name_fk FOREIGN KEY (p_id, p_name) 
        REFERENCES parent_tbl(p_id, p_name) -- 참조하고자 하는 프라이머리키가 복합키면 외래키도 복합키여야한다.
);

INSERT INTO child_tbl (c_id, p_id, p_name) VALUES (5000, 1000, '한나나'); -- 부모 복합키 내용에 맞게 넣어줘야함
SELECT * FROM child_tbl;

DROP TABLE child_tbl;

--------------------------------------------------------------------------------
CREATE TABLE child_tbl(
    c_id NUMBER(5) PRIMARY KEY,
    c_gender CHAR(1), 
    c_mgr NUMBER(5), 
    CONSTRAINT child_c_mgr_fk FOREIGN KEY (c_mgr)  -- 자기자신 외래키
        REFERENCES child_tbl (c_id) ON DELETE CASCADE, -- 부모가 지워지면 같이 삭제
    CONSTRAINT child_c_gender_ck CHECK (UPPER(c_gender) IN ('M', 'F')) -- 체크(연산필요) 
);

--INSERT INTO child_tbl (c_id, c_gender) VALUES ( 1000, 'A'); -- 체크 제약조건이 만족하지 못함
INSERT INTO child_tbl (c_id, c_gender) VALUES ( 1000, 'M');
INSERT INTO child_tbl (c_id, c_gender) VALUES ( 2000, 'F');
INSERT INTO child_tbl VALUES ( 3000, 'M', 1000);
INSERT INTO child_tbl VALUES ( 4000, 'M', 3000);
INSERT INTO child_tbl VALUES ( 5000, 'F', 1000);
--INSERT INTO child_tbl VALUES ( 6000, 'F', 8000); --외래키에 없는 부모값을 넣음 안됨

DELETE FROM child_tbl WHERE c_id = 1000; --ON DELETE CASCADE옵션이 있으면 프라이머리키 값을 지우면 그 값을 참조하는 외래키도 같이 지워짐

SELECT * FROM child_tbl;
DROP TABLE child_tbl;

--------------------------------------------------------------------------------
CREATE TABLE test_tbl(
    t_id NUMBER(5),
    CONSTRAINT test_t_id_fk FOREIGN KEY (t_id)
        REFERENCES child_tbl (c_id)
);

INSERT INTO test_tbl VALUES (2000); 
-- 외래키 들고 있는 다른 테이블에서 다 동의하지 않으면 외래키로 사용되는 프라이머리키는 삭제불가능
DELETE FROM child_tbl;
--프라이머리키를 지우고 싶으면 그것을 참조하고 싶은 외래키 다 찾아서 지운 후 지워야한다.

SELECT * FROM test_tbl;
DROP TABLE test_tbl;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--제약조건 추가/삭제
--제약조건 추가
ALTER TABLE parent_tbl
ADD CONSTRAINT parent_p_gender_ck CHECK (UPPER(p_gender) IN ('M', 'F'));

--NOT NULL추가
ALTER TABLE parnet_tbl 
MODIFY p_gender CHAR(1) NOT NULL; --테이블안 데이터가 다 차있거나 테이블 자체가 비어져잇어야함

--pk삭제
ALTER TABLE parent_tbl
DROP PRIMARY KEY;

--uk삭제
ALTER TABLE parent_tbl
DROP UNIQUE(p_id);

--check삭제
ALTER TABLE parent_tbl
DROP CONSTRAINT parent_p_gender_ck;

--------------------------------------------------------------------------------
--비활성화 => 원래 데이터 건들지 않는다.
ALTER TABLE child_tbl
DISABLE PRIMARY KEY CASCADE;  --CASCADE옵션사용해서 외래키 비활성화

INSERT INTO test_tbl VALUES(12345); -- 제약조건이 비활성화 되어있어서 없는것과 마찬가지 / 아무값이나 들어갈 수 있게 되었다.
INSERT INTO child_tbl (c_id) VALUES ( null ); --원래는 id는 프라이머리키라서 null안되지만 비활성화 해서 됨
SELECT * FROM child_tbl;

--활성화
ALTER TABLE child_tbl
ENABLE PRIMARY KEY; -- 널값 존재해서 활성화 안됨

DELETE child_tbl WHERE c_id IS NULL;

ALTER TABLE child_tbl
ENABLE CONSTRAINT child_c_mgr_fk; -- 외래키 활성화

--------------------------------------------------------------------------------
--문제5)

CREATE TABLE y_dept(
    id NUMBER(7) PRIMARY KEY,
    name VARCHAR2(25)
);

CREATE TABLE y_emp(
    id NUMBER(7) PRIMARY KEY,
    last_name VARCHAR2(25),
    first_name VARCHAR2(25),
    dept_id NUMBER(7),
    CONSTRAINT y_emp_dept_id_fk FOREIGN KEY (dept_id)
        REFERENCES y_dept (id)
);

/* 
--문제 5-1)
y_dept테이블에 다음 컬럼 추가 
addr varchar2(30)
*/
ALTER TABLE y_dept
ADD (
    addr VARCHAR2(30)
);

/*
--문제 5-2)
y_dept테이블 name컬렘에 not null제약조건 추가
*/
ALTER TABLE y_dept
MODIFY name VARCHAR2(25) NOT NULL;

DESCRIBE y_dept;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--VIEW
CREATE VIEW v_emp10
AS 
    SELECT employee_id, first_name, hire_date, salary
    FROM employees
    WHERE department_id = 10;

SELECT * FROM v_emp10; 
/* v_emp10 =  (SELECT employee_id, first_name, hire_date, salary
               FROM employees
               WHERE department_id = 10;)  
    뷰 = 서브쿼리
    우리가 만드는 모든 서브쿼리는 뷰가 될수 있따.*/
               
--기존에 있는 뷰 대체
CREATE OR REPLACE VIEW v_emp10 (empId, ename, hire_d, sal, dept)
AS 
    SELECT employee_id, first_name, hire_date, salary, department_id
    FROM employees
    WHERE department_id = 10;
    
SELECT * FROM v_emp10; 

DROP VIEW v_emp10;

--뷰 DML
--insert => nn조건 많아서 안됨
INSERT INTO v_emp10 VALUES(1234, 'Hong', TO_DATE('2022-12-01', 'yyyy-MM-dd'), 10000);
--UPDATE
UPDATE v_emp10
  SET ename = 'Hong';  -- where절이 없다. => 뷰가 들고있는 값이 하나밖에 없어서 가능 
                       --                  뷰가 접근하는 것들만 바뀐다.
DELETE v_emp10;--delete => pk있어서 안됨

SELECT * FROM v_emp10; 

--뷰 옵션
UPDATE v_emp10
   SET dept = 20
WHERE empid = 200;  --> 범위밖으로 업데이트 돼서 안오임

SELECT * FROM v_emp10;  
ROLLBACK;

--WITH CHECK OPTION;
CREATE OR REPLACE VIEW v_emp10 (empId, ename, hire_d, sal, dept)
AS 
    SELECT employee_id, first_name, hire_date, salary, department_id
    FROM employees
    WHERE department_id = 10
    WITH CHECK OPTION;

UPDATE v_emp10
   SET dept = 20 -- 체크옵션때문에 안됨
WHERE empid = 200;

UPDATE v_emp10
   SET ename = 'Shin'
WHERE empid = 200;

SELECT * FROM v_emp10;  
ROLLBACK;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--인덱스
CREATE INDEX employees_sal_idx
ON employees (salary); -- ON + 어느 테이블 어느 컬럼을 인덱스로 만들건지 정해야한다. 
                       -- 여러개 가능(두개의 컬럼 복합해서 인덱스 만들수도 있다. => 복합키 생각
DROP INDEX employees_sal_idx;
