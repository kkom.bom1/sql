/*
그룹함수 : 100개의 행에 하나의 값 ex)최대값
          지정한 테이블 모든행에 대해 특정한 값을 도출
*/

--group by 는 정렬이 아니라 분류시키는것.
--order by 는 무조건 마지막
SELECT department_id, AVG(salary)
FROM employees
WHERE department_id IS NOT NULL
GROUP BY department_id
ORDER BY 2;

--------------------------------------------------------------------------------
--COUNT (행의 갯수 확인)
--테이블이 가지고 있는 모든행의 갯수 확인할때 많이 쓴다. 
SELECT COUNT(*) 
FROM employees;

SELECT COUNT(commission_pct), COUNT(salary)
FROM employees;

--커미션 널값을 가진 행만 count
SELECT COUNT(*)
FROM employees
WHERE commission_pct IS NULL;

--count안에서의 distinct는 널값을 제외시킴
SELECT COUNT (department_id), COUNT(DISTINCT department_id)
FROM employees;

--distinct를 단독으로 쓸때는 널값포함
SELECT DISTINCT department_id
FROM employees;


--------------------------------------------------------------------------------
--AVG/SUM (숫자타입만 가능, 널값 무시)
--널값이 포함되는냐 안되는냐에 따라 AVG값이 달라진다.
SELECT AVG(salary), AVG(commission_pct)
FROM employees;

SELECT (SUM(commission_pct)/COUNT(*)) 평균연산1,  --널값행 포함(해당 테이블이 널값이 포함되었다면 이렇게 사용하면 안된다)
       (SUM(commission_pct)/COUNT(commission_pct)) 평균연산2,  --널값행 제외
        AVG(commission_pct) -- 널값행 제외
FROM employees;


--max/min (모든타입 가능 -> 문자,날짜도 정렬가능)
--ex) min => 100명중 가장 연차가 높은사/ A
--ex) max => 가장연차가 낮은 사람/ Z
SELECT MIN(salary), MAX(salary), MIN(commission_pct), MAX(commission_pct)
FROM employees;

SELECT MIN(first_name), MAX(first_name)
FROM employees;

SELECT MIN(hire_date) 처음입사, MAX(hire_date) 가장최근입사
FROM employees;

--널값 처리
SELECT AVG(salary), AVG(NVL(salary,0))
FROM employees;

SELECT AVG(commission_pct), AVG(NVL(commission_pct,0)),
      SUM(commission_pct), SUM(NVL(commission_pct,0))
FROM employees;


--------------------------------------------------------------------------------
--데이터 그룹화
--GROUP BY
--그룹함수는 개별행에 대해서는 쓸 수 없다.(전체를사용해 하나의 값으로 나오는거니까 -> 그룹함수와 개별행과는 연관성이 없음)
-- => select절에 있는건 groub by절에도 있어야한다.(groub by 에 있는건 select절에 생략 가능)
SELECT department_id, SUM(salary)
FROM employees
GROUP BY department_id
ORDER BY SUM(salary) DESC;

--부서안 각 업무별 총합
SELECT department_id, job_id, SUM(salary)
FROM employees
GROUP BY department_id, job_id  -- GROUB BY 대분류(department_id), 중분류(job_id), 소분류
                                -- 현재 테이블 안에서 부서간에 중복되는 부서가 없어서 지금은 대분류 중분류 순서 바껴도 값 영향 ㄴ
ORDER BY department_id DESC;

--HAVING절: 그룹화 시킨 후 각 그룹별로 조건을 걸고 싶을때 => 조건에 만족한 그룹만 남아서 SELECT절에 있는 그룹함수 결과 출력
--각 부서 중 평균이 60000보다 높은 부서의 평균을 보여줌
SELECT department_id, AVG(salary)
FROM employees
GROUP BY department_id
HAVING SUM(salary) > 60000; -- HAVING절에 사용한 그룹함수 반드시 SELECT절에 쓸 필요없다.

--GROUP BY : 소그룹화 시킬때 사용하는 기준
--HAVIGN : 그룹화 후에 적용할 조건 => 그룹별로 조건을 확인해야하니까 그룹함수를 사용하여 조건 줄때가 많다. 
--WHERE절에서 그룹화 조건을 주는건 의미가 없다.
SELECT department_id, AVG(salary)
FROM employees
WHERE SUM(salary) > 60000 -- where 절은 employees테이블에서 값을 가져오는것(테이블 한행씩 비교하면서 조건실행)
                          -- 그룹함수 기준으로 조건을 걸 수 없다. (참 거짓인지 아닌지 알아볼 보기가 없음)
GROUP BY department_id;
-- 그룹에 대한 조건이 필요한 경우 GROUP BY 와 HAVING절에 모두 필요하다.

--중첩
SELECT department_id, SUM(salary)
FROM employees
GROUP BY department_id;

SELECT MAX(SUM(salary)) --안쪽에 있는 그룹함수의 결반을 기반으로 다시 그룹함수
FROM employees
GROUP BY department_id;


--현재회사의 근무중에 모든 사원의 수 
--COUNT => 행의 수 
SELECT COUNT(*)
FROM employees;
 
--업무별 사원 수 표시
SELECT job_id, COUNT(*)
FROM employees
GROUP BY job_id;

--모든 사원의 급여 최고액, 최저액, 총액 및 평균액 (정수로 반올림해서) 출력
SELECT ROUND(MAX(salary)) 최고액,
       ROUND(MIN(salary)) 최저액,
       ROUND(SUM(salary)) 총액,
       ROUND(AVG(salary)) 평균액
FROM employees;
--그룹함수는 GROUP BY절이 필수가 아니다

--업무별 사원의 급여 최고액, 최저액, 총액, 평균액 표시하되 0이하값 버림
SELECT job_id 업무,
       TRUNC(MAX(salary)) 최고액,
       TRUNC(MIN(salary)) 최저액,
       TRUNC(SUM(salary)) 총액,
       TRUNC(AVG(salary)) 평균액
FROM employees
GROUP BY job_id;

--관리자 번호 및 해당 관리자에 속한 사원의 최저 급여를 표시
--단 관리자를 알 수 없는 사원 및 각 관리자에 속한 사원의 최저급여가 6000미만 그룹 제외
--그리고 급여에 대해 내림차순
SELECT manager_id as "관리자 번호",
       MIN(salary) as "최저급여"
FROM employees
WHERE manager_id IS NOT NULL
GROUP BY manager_id
HAVING MIN(salary) >= 6000
ORDER BY MIN(salary) DESC;


--------------------------------------------------------------------------------
--join
/*
--오라클 구문(오라클 전용 조인) : from 절에 테이블이 정렬, where에 조인의 조건을 쓴다.
- equl join : '같다(=)'라는 조건이 들어간다. (조건이 만족하지 않는 경우 누락)
- non-equl join : '같다(=)' 등호 이외의 조건이 들어간다. 
- outer join : equl join 기반, 조건을 만족하지 않는 경우도 포함
              (조건만족하지 않는 경우 표기(+) => 누락된 쪽을 표기한다.)
- self(재귀) join : equl join 기반, 본인을 join -> 조건이 중요
*/

--Equl(=) join
--조인 조건 기반으로 각 행을 묶어 하나의 테이블로 만든다.
--조건에 컬럼의 이름이 같으니 별칭(해당 테이블 첫글자)을 사용한다.
SELECT e.*, d.*
FROM employees e, departments d
WHERE e.department_id = d.department_id; --조인 조건(각 테이블의 같은 컬럼)

--Non-Equl join: between을 주로 많이 쓴다.
--급여로만 어떤일을 하는지 추측(job테이블의 최소 최대 급여를 통해)
SELECT e.employee_id, e.salary, j.job_title
FROM employees e, jobs j
WHERE e.salary BETWEEN j.min_salary AND j.max_salary
ORDER BY 1;

--(Equl/Non-Equl)join => 교집합이 기본. / 조건에 맞지 않으면 누락시킴 
--조인 조건에 만족하지 않지만 다 보여주고 싶으면 => outer join
--Outer join: 조건에 만족하지 않아도 보여줌(누락된 정보 추가)
--(누락된 정보 => 안보이는 쪽)
--(+)가 붙은쪽이 누락된쪽 조건이 만족한 부분만 출력/ 안붙은 쪽은 조건 안맞아도 100퍼 출력
--=> (+)는 정보가 누락된 쪽을 표시하는 용도임
SELECT COUNT(DISTINCT e.employee_id) emp, COUNT(DISTINCT d.department_id) dept
FROM employees e, departments d
WHERE e.department_id = d.department_id(+); 

SELECT COUNT(DISTINCT e.employee_id) emp, COUNT(DISTINCT d.department_id) dept
FROM employees e, departments d
WHERE e.department_id(+) = d.department_id; 

--Self join : 자기를 조인, 한테이블안에 프라이머리키와 외래키가 존재할때 사용
SELECT e.employee_id 사원, e.first_name 사원이름, 
       m.employee_id 매니저번호, m.first_name 매니저이름
FROM employees e, employees m
-- 사원정보를 보여주는 e의 매니저 아이디가 매니저 정보를 보여주는 m의 사원아이디가 같다.( m.employee_id 매니저번호)
--WHERE e.manager_id = m.employee_id
WHERE e.manager_id = m.manager_id -- e,m 둘다 148번 매니저에게 소속된 사원정보가 조인됨.(e의 매니저 아이디를 매니저로 가진 m의 사원정보가 조인됨)
  AND e.manager_id = 148
ORDER BY 1;


--------------------------------------------------------------------------------
--SQL-1999
