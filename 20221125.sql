--날짜함수
--오늘부터 10일 뒤의 날짜 
--날짜 함수에 10을 더하는건 단순히 더하는게 아니라 일수가 변함
SELECT  sysdate as today, sysdate + 10 as "After 10 Day"
FROM dual;

--시간도 같이 빠져서 정수로 나오지 않는다. => TRUNMC
SELECT TRUNC(TO_DATE('2022-12-31', 'YYYY-MM-DD') - sysdate) "올해 남은 날짜"
FROM dual;

SELECT TO_DATE('2022-12-31', 'YYYY-MM-DD') - sysdate "올해 남은 날짜"
FROM dual;

--두 날짜 사이의 월수를 반환
--MONTHS_BETWEEN : (단위가 월) 사이값
SELECT sysdate Today, hire_date 입사일, MONTHS_BETWEEN(sysdate, hire_date)
FROM employees;

--연차구하기
SELECT sysdate Today, hire_date 입사일, 
    TRUNC(MONTHS_BETWEEN(sysdate, hire_date)) || '개월' "연차(개월)",
    TRUNC(MONTHS_BETWEEN(sysdate, hire_date)/12) || '년 ' ||
    TRUNC(MOD(MONTHS_BETWEEN(sysdate, hire_date),12)) || '개월' "연차(년 개월)"
FROM employees;

-- ADD_MONTHS : 현재날짜 기점으로 몇개월 후 -> 날짜로 반환되니까 반올림, 버림 할 필요없음
--              -6은 6개월 뺌
SELECT sysdate Today, 
    ADD_MONTHS(sysdate, 6) "After 6 Month"
FROM dual;

--LAST_DAY
--NEXT_DAY : 오늘로 부터 돌아오는 월요일
-- 날짜로 돌아오니까 날짜연산 사용가능
SELECT sysdate today,
    LAST_DAY(sysdate) "Month Last Day",
    NEXT_DAY(sysdate, '월'),
    NEXT_DAY(sysdate, '월') - sysdate "월요일까지 남은 날"
FROM dual;

--ROUND : 날짜타입으로 받아서 반올림 후 날짜타입으로 반환
SELECT sysdate today,
    ROUND(sysdate, 'DD') as "가까운 일",
    ROUND(sysdate, 'DAY') as "가까운 주 일요일",
    ROUND(sysdate, 'MON') as "가까운 달",
    ROUND(sysdate, 'YEAR') as "가까운 년도"
FROM dual;

--TRUNC
SELECT sysdate,
    ROUND(sysdate, 'MON'), --반올림
    TRUNC(sysdate, 'MON') --버림 (지금 지정한 날짜(기준)에서 일자와 시간 초기화(버림))
FROM dual;

--오버로딩 : 똑같은 이름이지만 내가 지정한 것에 따라 결과가 다름
--오버라이딩 : 상속관계에서 부모꺼를 다시 씀


--------------------------------------------------------------------------------
--날짜레 TO_CHAR : 날짜를 문자타입으로 출력 (내가 정해진 형식으로 한 줄이 하나인 값으로 출력)
--변환함수(문자는 ""표시)
SELECT TO_CHAR(sysdate, 'yyyy"년" MM"월" DD"일"') --날짜타입이 아니라 문자타입
FROM dual;

--2005년에 입사한 사원들 출력
SELECT employee_id, first_name, hire_date
FROM employees
WHERE TO_CHAR(hire_date, 'yyyy') = '2005';

SELECT TO_CHAR(hire_date, 'yyyy "년" DDSPTH Month HH:MI:SS PM')
FROM employees
WHERE department_id = 50;

--2022년 TWENTY-FIFTH 11월 11:05:10 오전
SELECT TO_CHAR(sysdate, 'yyyy"년" DDSPTH Month HH:MI:SS PM')
FROM dual;

--fm 공백이나 0을 제거
SELECT TO_CHAR(sysdate, 'fmyyyy"년" DDSPTH Month mon dd day dy HH24:MI:SS PM')
FROM dual;

--숫자에 TO_CHAR ( 정해준 형식으로 출력)
SELECT first_name, TO_CHAR(salary, 'L999,999.99') salary
FROM employees;

--$는 문자타입 -> 숫자로 변환해야한다. / 값은 숫자로 넣어주고 표기할때 to_char로 변환해줘야한다.
SELECT TO_CHAR((12*123), 'L99,999')
FROM dual;


--------------------------------------------------------------------------------
-- TO_NUMBER 
--타입만 문자일뿐 값이 숫자랑 같으면 그냥 쓰면됨 (포멧형식 필요 ㄴㄴ)
SELECT TO_NUMBER('10000') + 10000
FROM dual;

--정해진 형식으로 읽어서 숫자로 변환
SELECT TO_NUMBER('$1000', '$9999') +10000
FROM dual;

SELECT TO_NUMBER('$10,000', '$99,999') + 1234
FROM dual;

--------------------------------------------------------------------------------
--TO_DATE
--포멧을 정확하게 인지켜주면 인지를 못한다.
SELECT TO_DATE('2022년, 11월', 'YYYY"년", MM"월"')
FROM dual;

SELECT TO_DATE('99/05/23'),
    TO_CHAR(TO_DATE('99/05/23'), 'YYYY-MM-dd'),  --정해진 날짜 포맷이라서 형식모델은 생략가능(기본적으로 RR형식으로 읽음)
    TO_CHAR(TO_DATE('99/05/23', 'YY/MM/dd'), 'YYYY-MM-dd') -- yy는 현재 세기를 기반으로 읽는다. => 이전세기도 입력하려면 RR형식사용
FROM dual;

SELECT TO_DATE('11월 11, 99', 'MM"월" DD, RR')
FROM dual;

--각 사원의 이름과 근무 달 수 출력
--근무 달수 계산 방식 (입사일로부터 현재까지의 달 수, 반올림, 정스)
-- MONTHS_WORK로 컬럼명
-- 근무달 수가 많은 사람부터 보이도록 정렬
--ROUND(MONTHS_BETWEEN(sysdate, hire_date), 0) MONRHS_WORK => 근무달수
SELECT first_name, ROUND(MONTHS_BETWEEN(sysdate, hire_date), 0) MONTHS_WORK
FROM employees
ORDER BY MONTHS_WORK DESC;


-- 사원의 이름, 입사일, 급여 검토일, 급여 검토일의 요일을 출력하려고 합니다.
-- 급여 검토일 : 입사일로부터 여섯 달이 경과한 후 첫번째 월요일.

-- => 입사일로부터 여섯 달이 경과(입사일 + 6개월이라는 뜻) : ADD_MONTHS(hire_date, 6) = 2023-05-25
-- => 이후 첫번째 월요일 : NEXT_DAY( ADD_MONTH(hire_date, 6), '월')

SELECT first_name,
        hire_date,
        NEXT_DAY(ADD_MONTHS(hire_date,6),'월') "급여 검토일",
        TO_CHAR(NEXT_DAY(ADD_MONTHS(hire_date,6),'월'), 'DAY') "급여 검토일의 요일"
FROM employees;

-- 회사의 급여일은 매월 말일입니다. 각 사원이 처음 급여를 받은 날짜와 해당 요일 출력
SELECT first_name,
        LAST_DAY(hire_date) "첫 급여일",
        TO_CHAR(LAST_DAY(hire_date),'DAY') "첫 급여받은 요일"
FROM employees;

-- 회사의 급여일은 매월 25일입니다. 각 사원이 처음 급여를 받은 날짜와 해당 요일 출력
-- 내가 풀어보자..

--------------------------------------------------------------------------------
--NULL을 다루는 함수들

--NVL(컬럼, 대체할 값) -- NULL값만 바꾸고 싶을 때. 대체값은 모든 타입 가능하지만 컬럼이 가질 수 있는 값으로.
--NVL2(컬럼, 널이 아닐 경우, 널일 경우) -- 값 완전히 대체시킴.
--NULLIF(컬럼1, 컬럼2) -- 두 개의 컬럼 값이 같을 때 NULL줌. 다를 경우 컬럼1의 값을 반환. 컬럼1의 값은 NULL이면 안됨.
--COALESE(컬럼1, 컬럼2, .. 컬럼 n) -- 값을 무한정 가질 수 있음. 나열된 것 중에 첫번째로 값이 있는 것 반환. 
--                                    널이 있으면 다음 컬럼으로 넘어감. 앞이 모두 널이면 그냥 마지막 값 반환시킴.(널이어도)

-- NVL
SELECT first_name, salary, NVL(commission_pct,0)
FROM employees;

SELECT first_name, salary, (salary+salary*NVL(commission_pct,0))*12 "연봉"
FROM employees;

-- NVL2
SELECT first_name, NVL2(commission_pct,'대상자','대상자가 아님') "수당 대상"
FROM employees;

-- NULLIF(진짜 잘 안씀...) : 두 개의 값 같으면 NULL, 다르면 첫 번째 컬럼 값.
SELECT NULLIF('A','A'), NULLIF('A','a')
FROM dual;

-- 쓸려면 쓸 수는 있다..
SELECT NVL(NULLIF('A','A'),'다름','같음')
FROM dual;

-- COALESCE : 널이 아닌 컬럼을 찾고 싶으면 사용
SELECT COALESCE(NULL, NULL, 10, 100, NULL)
FROM dual;


--------------------------------------------------------------------------------
-- 조건문
SELECT first_name,
        job_id,
        CASE SUBSTR(job_id,1,2)
            WHEN 'SA' THEN '영업과 관련된 부서입니다.'
            WHEN 'IT' THEN '개발과 관련된 부서입니다.'
            ELSE '기타 부서입니다.'
        END 비고 
        -- CASE문에는 마지막에 꼭 END써준다.
FROM employees;

-- 위와 같은 내용을 DECODE로 함.
SELECT first_name,
        job_id,
        DECODE(SUBSTR(job_id,1,2), 'SA', '영업과 관련된 부서입니다.',
                                    'IT', '개발과 관련된 부서입니다.',
                                    '기타 부서입니다.') 비고
FROM employees;

SELECT first_name,
        salary,
        CASE WHEN salary <= 10000 THEN salary * 1.2 -- 10000보다 작거나 같다
             WHEN salary <= 15000 THEN salary * 1.15 -- 10000초과, 15000작거나같다
             WHEN salary <= 20000 THEN salary * 1.1
             ELSE salary
        END "인상된 급여"
FROM employees;

SELECT x FROM number_tbl;

SELECT x,
        CASE MOD(x,2)
            WHEN 0 THEN '짝수'
            ELSE '홀수'
        END 홀짝
FROM number_tbl;

/*
    업무      : 등급
    AD_PRES  : A
    ST_MAN   : B
    IT_PROG  : C
    SA_REP   : D
    ST_CLERK : E
    Etc      : 0
*/

SELECT first_name,
        job_id,
        CASE job_id
            WHEN 'AD_PRES' THEN 'A'
            WHEN 'ST_MAN' THEN 'B'
            WHEN 'IT_PROG' THEN 'C'
            WHEN 'SA_REP' THEN 'D'
            WHEN 'ST_CLERK' THEN 'E'
            ELSE '0' -- 그냥 숫자로 하면 x. 다 문자열이라서 문자열로 넘겨줌.
        END 등급
FROM employees;

SELECT job_id,
        DECODE(job_id, 'AD_PRES', 'A',
                        'ST_MAN', 'B',
                        'IT_PROG', 'C',
                        'SA_REP', 'D',
                        'ST_CLERK', 'E',
                        '0') 등급
FROM employees;

-- 사원의 이름과 커미션 출력.
-- 커미션이 없는 경우 'No Commission'라고 표시하세요.
-- 커미션을 출력하는 컬럼의 별칭은 'COMM'으로 지정.

SELECT first_name,
--        NVL2(commission_pct,TO_CHAR(commission_pct),'No Commission') "COMM"
        NVL(TO_CHAR(commission_pct, '0.99'), 'NO Commission') comm
FROM employees;
            

