--문제1)
SELECT d.department_id 부서번호, d.department_name 부서이름, l.city 도시명
FROM departments d JOIN locations l
  ON d.location_id = l.location_id;
  
--------------------------------------------------------------------------------  
--문제2) 
SELECT e.employee_id 사번, e.first_name 사번이름, d.department_id 소속부서번호, d.department_name 소속부서이름
FROM employees e LEFT OUTER JOIN departments d
ON e.department_id = d.department_id;

--------------------------------------------------------------------------------
--문제3) 
SELECT department_id 부서번호, 
       COUNT(employee_id) "부서별 근무인원 수",
       TRUNC(AVG(salary), 0) "부서별 평균급여"
FROM employees
GROUP BY department_id;

--------------------------------------------------------------------------------
--문제4)
SELECT employee_id 사원번호, first_name 이름, job_id 업무
FROM employees
WHERE employee_id IN (SELECT e.employee_id
                     FROM employees e JOIN departments d
                     ON e.department_id = d.department_id
                     WHERE UPPER(d.department_name) = 'IT');
         
--------------------------------------------------------------------------------            
--문제5)
CREATE TABLE prof (
    profno NUMBER(4, 0) PRIMARY KEY,
    name VARCHAR2(15) NOT NULL,
    id VARCHAR2(15) NOT NULL,
    hiredate DATE,
    pay NUMBER(4, 0)
);

--------------------------------------------------------------------------------
--문제6) 
--(1)
INSERT INTO prof VALUES (1001, 'Mark', 'm1001', TO_DATE('07/03/01','yy/MM/dd'), 800);
INSERT INTO prof VALUES (1003, 'Adam', 'a1003', TO_DATE('11/03/02','yy/MM/dd'), null);
commit;

--(2)
UPDATE prof SET pay = 1200 WHERE profno = 1001;

--(3)
DELETE prof WHERE profno = 1003;
commit;

--------------------------------------------------------------------------------
--문제7)
--(1)
CREATE INDEX prof_name_idx
ON prof (name);

--(2)
ALTER TABLE prof
DROP PRIMARY KEY;

ALTER TABLE prof
ADD CONSTRAINT prof_no_pk PRIMARY KEY(profno);

--(3)
ALTER TABLE prof
ADD gender CHAR(3);

--(4)
ALTER TABLE prof
MODIFY name VARCHAR(20);

--------------------------------------------------------------------------------
--문제8)
--(1)
CREATE VIEW prof_vu (pno, pname, id)
AS 
    SELECT profno, name, id
    FROM prof;
    
--(2)
CREATE OR REPLACE VIEW prof_vu (pno, pname, id, hiredate)
AS 
    SELECT profno, name, id, hiredate
    FROM prof;
    
--(3)
CREATE SYNONYM p_vu
FOR prof_vu;

--------------------------------------------------------------------------------
--문제9)


--------------------------------------------------------------------------------
--문제10)
--(1)
CREATE USER haksa
IDENTIFIED BY yd2460;

--(2)
GRANT CONNECT, RESOURCE
TO haksa;

--(3)
GRANT CREATE VIEW, CREATE SYNONYM
TO haksa;

--(4)
GRANT SELECT ON hr.departments
TO haksa;


