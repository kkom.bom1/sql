SELECT d.department_id, d.department_name, r.region_name
FROM departments d, locations l, countries c, regions r
WHERE d.location_id = l.location_id
  AND l.country_id = c.country_id
  AND c.region_id = r.region_id;

--SQL 표준 조인
SELECT d.department_id, d.department_name, r.region_name
FROM departments d
JOIN locations l ON d.location_id = l.location_id 
JOIN countries c ON l.country_id = c.country_id 
JOIN regions r ON c.region_id = r.region_id;

--------------------------------------------------------------------------------
/* [ SQL 표준 JOIN <-> ORACLE 전용 JOIN ]
SQL 표준 join 
- CROSS JOIN(교차조인) : 두 테이블 사이 조건은 없다.
                        두개의 주사위를 던졌을때 나오는 모든 경우의 수 처럼 두테이블 조합시 모든 경우를 요구
                        <-> ORACLE조인의 카테시안 프로덕트
- NATURAL JOIN(자연조인) : 두테이블 사이 동일한 이름의 컬럼이 조건 <-> EQUL JOIN
- (INNER) JOIN ~ ON <-> EQUL JOIN(같다) / NON-EQUL JOIN(같다 이외 모든 조건)/ SELF JOIN 
- OUTER JOIN (LEFT|RIGHT|FULL) <-> OUTER JOIN(LEFT|RIGHT)
  LEFT OUTER JOIN <-> OUTER JOIN 오른쪽 (+)   
  RIGHT OUTER JOIN <-> OUTER JOIN 왼쪽 (+)
  FULL OUTER JON <-> X
*/

--------------------------------------------------------------------------------
--[CROSS JOIN : 카테시안 프로덕트] ==> 교집합의 조건이 없는 경우 
--CROSS JOIN 
--조건이 필요없다. 그냥 조합 (해석 ㄴㄴ)
SELECT j.*, r.*
FROM jobs j CROSS JOIN regions r;

--카테시안 프로덕트
SELECT j.*, r.*
FROM jobs j, regions r;

--------------------------------------------------------------------------------
--[NATURAL JOIN | JOIN ~ ON : EQUL JOIN]
--NATURAL JOIN => 두테이블에 똑같이 들어가는 컬럼에는 접두어 사용 ㄴㄴ 
--                이름이 같다면 둘이 똑같은 줄이라 생각하기 때문
--                (department_id, manager_id는 한컬럼만 존재한다. -> 어느테이블 것인지 알 수 없다.)
SELECT e.employee_id, e.first_name, department_id, d.department_name, manager_id
FROM employees e NATURAL JOIN departments d;

--ps) USING 절
-- natural join과 같지만 이름의 컬럼 중 하나만 선택하는것 => using절에 선언된 컬럼은 접두어가 없다. 
SELECT e.employee_id, e.first_name, manager_id, d.department_id, d.department_name
FROM employees e JOIN departments d USING(manager_id);

--(INNER) JOIN ~ ON => ★교집합★을 의미, 두 테이블의 똑같은 이름의 컬럼이 같다고 해줘야함(on)
--                     department_id 가 각각의 테이블에 종속된거라고 보기때문 반드시 접두어가 필요
SELECT e.employee_id, e.first_name, e.department_id, d.department_name, e.manager_id
FROM employees e INNER JOIN departments d
  ON e.department_id = d.department_id
  AND e.manager_id = d.manager_id;
  
SELECT e.*, d.* --department_id칸이 2개 있음
FROM employees e INNER JOIN departments d
 ON e.department_id = d.department_id;
 
--EQUL JOIN 
SELECT e.employee_id, e.first_name, e.department_id, d.department_name, e.manager_id
FROM employees e, departments d
WHERE e.department_id = d.department_id AND e.manager_id = d.manager_id;

--------------------------------------------------------------------------------
--[JOIN ~ ON : NON-EQUI JOIN] => 조인의 조건중에는 반드시 =(같다)만 존재하지 않음.
--JOIN ~ ON
SELECT e.employee_id, e.first_name, e.salary, j.job_title
FROM employees e JOIN jobs j
  ON e.salary BETWEEN j.min_salary AND j.max_salary;
  
--NON-EQUI JOIN
SELECT e.employee_id, e.first_name, e.salary, j.job_title
FROM employees e, jobs j
WHERE e.salary BETWEEN j.min_salary AND j.max_salary;

--------------------------------------------------------------------------------
--JOIN ~ ON : SELF JOIN 
--JOIN ~ ON
SELECT e.employee_id 사원번호, e.first_name 사원이름, m.first_name 매니저이름
FROM employees e INNER JOIN employees m
  ON e.manager_id = m.employee_id;
  
--SELF JOINSELECT 
SELECT e.employee_id 사원번호, e.first_name 사원이름, m.first_name 매니저이름
FROM employees e, employees m
WHERE e.manager_id = m.employee_id;

--------------------------------------------------------------------------------
--[ LEFT OUTER JOIN : OUTER JOIN 오른쪽(+)] //(+)가 반대로 붙는다.
--LEFT OUTER JOIN
SELECT e.employee_id, e.first_name, d.department_name
FROM employees e LEFT OUTER JOIN departments d
  ON e.department_id = d.department_id;
  
--OUTER JOIN 
SELECT e.employee_id, e.first_name, d.department_name
FROM employees e, departments d
WHERE e.department_id = d.department_id(+);

--------------------------------------------------------------------------------
--[ RIGHT OUTER JOIN : OUTER JOIN 왼쪽(+)] //(+)가 반대로 붙는다.
--RIGHT OUTER JOIN
SELECT e.employee_id, e.first_name, d.department_id 
FROM employees e RIGHT OUTER JOIN departments d
  ON e.department_id = d.department_id;
  
--OUTER JOIN
SELECT e.employee_id, e.first_name, d.department_id 
FROM employees e, departments d
WHERE e.department_id(+) = d.department_id;

--------------------------------------------------------------------------------
--FULL OUTER JOIN => left와 right의 합집합(UNION)
SELECT e.employee_id, e.first_name, d.department_name
FROM employees e FULL OUTER JOIN departments d
  ON e.department_id = d.department_id;

--------------------------------------------------------------------------------
-- 모든 사원의 사원번호, 사원이름, 부서이름, 부서가 있는 도시를 출력하세요
-- 단, 부서번호가 20번인 부서에 근무하는 사원을 출력하세요
SELECT e.employee_id 사원번호, e.first_name 사원이름, d.department_name 부서이름, l.city 도시
FROM employees e -- 기준이 되는 테이블
LEFT OUTER JOIN departments d ON e.department_id = d.department_id  -- (모든사원 => 왼쪽에 있는 employees테이블은 다 보여줌 )
JOIN locations l ON d.location_id = l.location_id
--사실 inner join을 한번이라도 쓰면 outer join의 값은 누락돼서 같이 쓰면 ㄴ
WHERE d.department_id = 20;

SELECT e.employee_id 사원번호, e.first_name 사원이름, d.department_name 부서이름, l.city 도시
FROM employees e, departments d, locations l 
WHERE e.department_id = d.department_id(+)
  AND d.location_id = l.location_id
  AND  d.department_id = 20;
  
  
--모든 부서의 이름과 위치(location_id), 각 부서에 근무하는 사원의 수를 표시
--사원이 없는 부서도 함께 표시하새요
--(사원이 없는 부서(없는 내용 표기)... -> 이런 말이 없으면 보통은 inner join 사용) 
SELECT d.department_name "부서 이름", d.location_id "부서 위치", COUNT(e.employee_id) "사원의 수"
FROM departments d LEFT OUTER JOIN employees e 
  ON d.department_id = e.department_id 
GROUP BY d.department_name, d.location_id
ORDER BY "사원의 수" ASC;


-- Administration 및 Executive 부서에는 어떤 업무가 있으며 
-- 해당 업무를 맡고 있는 사원수는 몇명인지 표시 
-- 단 인원이 가장 많은 업무부터 표시 
--INNER JOIN
SELECT d.department_name 부서, j.job_title 업무, COUNT(e.employee_id) 사원수
FROM departments d 
JOIN employees e 
  ON d.department_id = e.department_id
JOIN jobs j
  ON e.job_id = j.job_id
WHERE INITCAP(d.department_name) IN ('Administration', 'Executive')
GROUP BY d.department_name, j.job_title                                          
ORDER BY 사원수 DESC;


--------------------------------------------------------------------------------
--서브쿼리
--사원번호 105번인 사람의 업무를 하는 사람들이 받을 수 있는 최소듭여가 최대급여를 함께 출력
--단일 행 브쿼리 => 단일값만 반환/ 단일행 비교 연산자 뒤에 사용
--1) 사원번호 105번인 사람의 업무
SELECT job_id 
FROM employees
WHERE employee_id =  105;
--2)
SELECT job_title, min_salary, max_salary
FROM jobs
WHERE job_id IN (SELECT job_id 
                FROM employees
                WHERE employee_id =  105);


--다중행 서브쿼리
--업무(job_title)중에 'Sales'가 들어가는 업무에 종사하는 사원의 사원번호와 이름 급여를 출력하세요
-- 1) 업무(job_title)중에 'Sales'가 들어가는 업무
SELECT job_title
FROM jobs
WHERE UPPER(job_title) LIKE '%SALES%';
--2) 
SELECT employee_id, first_name, salary
FROM employees
WHERE job_id IN (SELECT job_id
                FROM jobs
                WHERE UPPER(job_title) LIKE '%SALES%');
            

--------------------------------------------------------------------------------
--단일행서브쿼리 문제              
-- <사원 번호가 200번인 사원의 급여>보다 많이 받는 직원의 사원번호, 이름, 부서번호, 업무, 급여를 출력
--사원번호 200번인 사원의 급여
SELECT salary
FROM employees
WHERE employee_id = 200;
--2)
SELECT employee_id, first_name, department_id, job_id, salary
FROM employees
WHERE salary >(SELECT salary
               FROM employees
               WHERE employee_id = 200);


-- <부서의 평균 급여가 제일 큰> 부서의 부서이름과, 매니저 번호 평균급여를 출력
--1) 부서의 평균 급여가 제일 큰
SELECT MAX(AVG(salary))
FROM employees
GROUP BY department_id;
--2)
SELECT d.department_name, d.manager_id, AVG(e.salary)
FROM departments d
JOIN employees e ON d.department_id = e.department_id
GROUP BY d.department_name, d.manager_id
HAVING AVG(e.salary) = (SELECT MAX(AVG(salary))  -- 이미 그룹화 시켜놨기때문 개개인의 급여 비교는 의미없음.
                        FROM employees
                        GROUP BY department_id);
                        

-- <가장 적은 급여>를 받는 직원의 (사원번호, 이름, 부서번호, 급여, 입사일)을 출력하세요
--1) <가장 적은 급여>
SELECT MIN(salary)
FROM employees;
--2) (사원번호, 이름, 부서번호, 급여, 입사일)을 출력 -> employees 테이블
SELECT employee_id 사원번호, first_name 이름, department_id 부서번호, salary 급여, hire_date 입사일
FROM employees
WHERE salary = (SELECT MIN(salary) FROM employees);
